// console.log("Hello Gel!");

// // Stats:
//             name:
//             level:
//             heath: level * 2
//             attack: level * 1.5

let trainer = {
	name: "Gel",
	age: 32,
	pokemon: ["Pikachu", "Mew", "Snorlax", "Togepi", "Jigglypuff"],

	friends: {
		Che: ["Eevee", "Charizard"],
		Seph: ["Ditto", "Charmander"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
};
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
trainer.talk();

function Pokemon(name,level){
// properties
this.name = name;
this.level = level;
this.health = level * 2;
this.attack = level * 1.5;
this.tackle = function(target){
	console.log(this.name + " tackled " + target.name)

// Mini activity 3

target.health -= this.attack	
console.log(target.name + " health is now reduced to " + target.health);

if(target.health <= 0){
	target.faint()
}
}
this.faint = function(){
	console.log(this.name + " fainted.")
}
};

let pikachu = new Pokemon ("Pikachu", 22);
console.log(pikachu);

let mew = new Pokemon ("Mew", 5);
console.log(mew);

let jigglypuff = new Pokemon ("Jigglypuff", 20);
console.log(jigglypuff);